const webpack = require('webpack');
const moduleCommon = {
    rules: []
};

const pluginsCommon = [
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
    })
];

const cookiesConfig = {
    entry: [`${__dirname}/src/cookies/index.js`],
    output: {
        path: `${__dirname}/extension/cookies/`,
        filename: "cookiesBundle.js"
    },
    plugins: pluginsCommon,
    module: moduleCommon,
    devtool: 'sourcemap'
};

const backgroundConfig = {
    entry: [`${__dirname}/src/background.js`],
    output: {
        path: `${__dirname}/extension/`,
        filename: "backgroundBundle.js"
    },
    plugins: pluginsCommon,
    module: moduleCommon,
    devtool: 'sourcemap'
}

module.exports = [
    backgroundConfig,
    cookiesConfig
];