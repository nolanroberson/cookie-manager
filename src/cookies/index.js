import go from '../common/go.js'
(async function() {

/* GLOBALS */
    const root = document.getElementById('root');
    const menu = document.getElementById('menu');
    const content = document.getElementById('content');
    const menuLink = document.getElementById('menuLink');

/* UTILITY FUNCTIONS */
    function toggleClass(element, className) {
        const classes = element.className.split(/\s+/);
        const length = classes.length;
        let i = 0;

        for(; i < length; i++) {
        if (classes[i] === className) {
            classes.splice(i, 1);
            break;
        }
        }
        // The className is not found
        if (length === classes.length) {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }

    function template(strings, ...keys) {
        return (function(...values) {
          var dict = values[values.length - 1] || {};
          var result = [strings[0]];
          keys.forEach(function(key, i) {
            var value = Number.isInteger(key) ? values[key] : dict[key];
            result.push(value, strings[i + 1]);
          });
          return result.join('');
        });
      }

/* UI BUILDER FUNCTIONS */
    /**
     * @param {Array<ContextualIdenity>} identities 
     * @return {Element} nav element
     */
    function buildNavigation(identities) {
        let fragment = location.hash.substr(1);
        if (fragment.length < 1) {
            fragment = 'Default';
        }
        const isSelected = (ident) => fragment == ident.name;
        const itemTmpl = template`
        <li class="pure-menu-item ${'class'}" style="background-color:${'bgColor'}">
            <a href="#${'name'}" class="pure-menu-link" style="color:${'color'}">${'name'}</a>
        </li>
        `;

        const src = `
        <div class="pure-menu menu-restricted-width">
        <span class="pure-menu-heading">Containers</span>
            <ul class="pure-menu-list">
            ${
                identities.map(ident =>
                    itemTmpl({
                        color: isSelected(ident) ? '#333' : ident.colorCode, 
                        bgColor: isSelected(ident) ? ident.colorCode : 'inherit',
                        class: isSelected(ident) ? 'pure-menu-selected': '',
                        name: ident.name
                    })
                ).join('')
            }
            </ul>
        </div>
        `;
        return src
    }

    function buildCookieTable(domain, cookies) {
        const src = `
        <h4 class="table-title">${domain} – ${cookies.length} Cookie${cookies.length > 1 ? 's' : '' } </h4>
        <div class="table-container">
            <table class='pure-table pure-table-bordered'>
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Path</td>
                    <td class="fill-column">Content</td>
                </tr>
            </thead>
            ${cookies.map(cookie =>
                `<tr>
                    <td>${cookie.name}</td>
                    <td>${cookie.path}</td>
                    <td>${cookie.value}</td>
                </tr>`
            ).join('')}
            </table>
        </div>
        `;
        return src;
    }

    function buildAllCookieTables(cookies) {
        if (cookies.length < 1) {
            return '<h2 class="content-subhead">There were no cookies found.</h2>';
        }

        const cookiesByDomain = new Map();
        cookies.forEach(cookie => {
            if (!(cookiesByDomain.get(cookie.domain) instanceof Array)) {
                cookiesByDomain.set(cookie.domain, []);
            }

            cookiesByDomain.get(cookie.domain).push(cookie);
        });
        const src = Array.from(cookiesByDomain.keys()).map(domain => 
            buildCookieTable(domain, cookiesByDomain.get(domain))
        ).join('');
        return src;
    }

    function buildErrorMessage(error) {
        const src = `
        <div>
            <h1>Error</h1>
            <p>${error.message}</p>
        </div>
        `;
        return src;
    }

    async function load() {
        // Event registration
        // Make the hamburger menu toggleable.
        menuLink.addEventListener('click', (e) => {
            const active = 'active';
            e.preventDefault();
            toggleClass(root, active);
            toggleClass(menu, active);
            toggleClass(menuLink, active);
        }, false);

        let identities, err;
        [identities, err] = await go(browser.contextualIdentities.query({}));
    
        if (err) {
            root.innerHTML = buildErrorMessage(err);
            return;
        }
    
        // Add the default container
        identities.unshift({
            cookieStoreId: 'firefox-default',
            color: 'grey',
            colorCode: '#ddd',
            icon: 'circle',
            iconUrl: 'resource://usercontext-content/circle.svg',
            name: 'Default'
        });
    
        menu.innerHTML = buildNavigation(identities);
    
        // Which container is selected
        // TODO: Handle fragments with special characters in them.
        let storeId;
        const fragment = location.hash.substr(1)
        
        if (fragment.length < 1) {
            storeId = 'firefox-default';
        } else {
            const match = identities.filter(ident => ident.name == fragment).shift();
            if (!match) {
                content.innerHTML = buildErrorMessage(new Error(`Container "${fragment}" does not exist.`));
                return;
            }
            else {
                storeId = match.cookieStoreId;
            }
        }
    
        // Get the cookies from the selected container's cookie store.
        let cookies;
        err = null;
        [cookies, err] = await go(browser.cookies.getAll({
            storeId: storeId
        }));
    
        if (err) {
            content.innerHTML = buildErrorMessage(err);
            return;
        }
        content.innerHTML = buildAllCookieTables(cookies);
    }

/* ENTRY POINT */
    window.addEventListener('hashchange', () => {
        load();
    }, false);
    load();
})();