/**
 * Golang style error handling for {Promises}.
 * If the {Promise} is fullfiled then the first item will be the fulfillment
 * value. The second item will be null.
 * If the {Promise} is rejected then the first item will be null. The second
 * item will be the rejection reason.
 * @param {Promise} promise 
 * @return tuple with fulfillment value and rejection reason.
 */
export default function go(promise) {
    return promise.then(data => {
        return [data, null];
    })
    .catch(err => [null, err]);
}