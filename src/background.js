browser.browserAction.onClicked.addListener(function() {
    let url = browser.extension.getURL("cookies/cookies.html")
    createOrSelectTab(url);
});

async function createOrSelectTab(url) {
    browser.tabs.create({
        url: url
    });
}