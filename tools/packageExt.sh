#!/usr/bin/env bash

echo "Packaging!"
dest=build
ext_name=Cookies
firefox_filename=${ext_name}_Firefox_Dev_1.0.0
chrome_filename=${ext_name}_Chrome_Dev_1.1.0
mkdir -p $dest
cd extension/
zip -r ${firefox_filename}.xpi *
mv ${firefox_filename}.xpi ../$dest/

echo "Done!"